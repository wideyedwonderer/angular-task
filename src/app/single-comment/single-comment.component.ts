import { CommentShareService } from './../helpers/comment-share.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Comment } from '../models/comment';
import { MatDialog } from '@angular/material';
import { AreYouSureComponent } from '../helpers/are-you-sure/are-you-sure.component';


@Component({
  selector: 'app-single-comment',
  templateUrl: './single-comment.component.html',
  styleUrls: ['./single-comment.component.css']
})
export class SingleCommentComponent implements OnInit {
  index: number;

  comments: Comment[] = [];
  comment: Comment;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly commentShareService: CommentShareService,
    public dialog: MatDialog
    ) {

   }

  ngOnInit() {
    this.index = Number(this.route.snapshot.paramMap.get('id'));
    this.comments = this.route.snapshot.data['comments'];
    this.comment = this.comments[this.index];
  }

  onClick() {
   const dialogRef = this.dialog.open(AreYouSureComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.comments.splice(this.index, 1);
        this.commentShareService.$comments.next(this.comments);
        this.router.navigate(['/comments/new']);
        }

      });
  }
  }


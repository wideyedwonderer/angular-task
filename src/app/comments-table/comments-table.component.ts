import { Comment } from './../models/comment';
import { Component, OnInit, Input, ViewChild, IterableDiffers, OnChanges, Output, EventEmitter } from '@angular/core';
import { MatTable, MatTableDataSource } from '@angular/material';
import { Subject, Observable } from 'rxjs';

@Component({
  selector: 'app-comments-table',
  templateUrl: './comments-table.component.html',
  styleUrls: ['./comments-table.component.css']
})
export class CommentsTableComponent implements OnInit {

  @Output()
  rowClickedEmitter: EventEmitter<number> = new EventEmitter();
  @Input()
  renderRows: Subject<boolean>;

  @Input()
  dataSource: Observable<Comment[]>;

  @ViewChild(MatTable, {static: false})
  table: MatTable<any>;

  displayedColumns: string[] = ['title', 'dateAdded', 'type'];
  ngOnInit(): void {
   this.renderRows.subscribe(() => {
     this.table.renderRows();
   });
  }

  onClick(index) {
    this.rowClickedEmitter.emit(index);
  }

}

import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HeaderAndFooterComponent } from './header-and-footer/header-and-footer.component';
import { NewCommentComponent } from './new-comment/new-comment.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import {
  MatFormFieldModule,
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatSelectModule,
  MatToolbarModule,
  MatSpinner,
  MatTableModule,
  MatDialogModule
} from '@angular/material';
import { CommentsTableComponent } from './comments-table/comments-table.component';
import { SingleCommentComponent } from './single-comment/single-comment.component';
import { AreYouSureComponent } from './helpers/are-you-sure/are-you-sure.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderAndFooterComponent,
    NewCommentComponent,
    MatSpinner,
    CommentsTableComponent,
    SingleCommentComponent,
    AreYouSureComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatSelectModule,
    MatToolbarModule,
    MatTableModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    AreYouSureComponent,
  ],
})
export class AppModule { }

import { CommentType } from './../models/comment-type';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Comment } from '../models/comment';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { CommentShareService } from '../helpers/comment-share.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-new-comment',
  templateUrl: './new-comment.component.html',
  styleUrls: ['./new-comment.component.css']
})
export class NewCommentComponent implements OnInit {
  commentTypes = Object.values(CommentType).filter((value) => typeof value === 'string');
  renderRows: Subject<boolean> = new Subject();
  newCommentForm = this.fb.group({
    title: [''],
    comment: ['', Validators.required],
    type: [null, Validators.required],
  });
  constructor(
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly commentShareService: CommentShareService
    ) { }
  lastCommentAdded: Comment = null;
  comments: Comment[] = [];
  ngOnInit() {
    this.commentShareService.$comments.pipe(take(1)).subscribe((comments) => {
      if (comments) {
        this.comments = comments;
        this.renderRows.next(true);
      }
    });
  }


  onSubmit() {
    const value: string = this.newCommentForm.controls['comment'].value;
    const type: CommentType = <CommentType>this.newCommentForm.controls['type'].value;
    const title: string = this.newCommentForm.controls['title'].value;
    const commentToAdd: Comment = new Comment(value, type, title);
    this.lastCommentAdded = commentToAdd;
    const date: Date = new Date();
    // date.getUTCDate
    this.comments.sort((a, b) => <any>a.getDateAdded() - <any>b.getDateAdded());
    this.comments.push(commentToAdd);
    this.renderRows.next(true);
    this.commentShareService.$comments.next(this.comments);
    this.newCommentForm.reset();
  }


  rowClicked(index) {
    this.router.navigate([`comments/${index}`]);
  }
}

import { Comment } from '../../models/comment';
import { CommentShareService } from '../comment-share.service';
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { delay } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FakeCommentsResolver implements Resolve<any> {

  comments: Comment[];
  constructor(private readonly commentService: CommentShareService) {
    this.commentService.$comments.subscribe((comments) => this.comments = comments)
  }

   resolve(): Observable<Comment[]> {
    return of(this.comments).pipe(delay(1000));
  }
}

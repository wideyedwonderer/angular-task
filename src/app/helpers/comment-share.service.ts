import { Comment } from './../models/comment';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentShareService {
  $comments: BehaviorSubject<Comment[]> = new BehaviorSubject(null);
  constructor() { }
}

import { MatDialogRef } from '@angular/material';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-are-you-sure',
  templateUrl: './are-you-sure.component.html',
})
export class AreYouSureComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AreYouSureComponent>) { }

  ngOnInit() {
  }

  onCancel() {
    this.dialogRef.close(false);
  }
  onConfirm() {
    this.dialogRef.close(true);
  }
}

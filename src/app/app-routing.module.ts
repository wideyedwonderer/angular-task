import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { NewCommentComponent } from './new-comment/new-comment.component';
import { SingleCommentComponent } from './single-comment/single-comment.component';
import { FakeCommentsResolver } from './helpers/fake-comments-resolver/fake-comments-resolver';

const routes: Routes = [
    { path: 'comments/new', component: NewCommentComponent},
    { path: 'comments/:id', component: SingleCommentComponent, resolve: {comments: FakeCommentsResolver}},
    { path: '**', redirectTo: 'comments/new'},
  ];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
  })
  export class AppRoutingModule {}

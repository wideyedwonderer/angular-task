import { CommentType } from './comment-type';
export class Comment {
    private value: string;
    private dateAdded: Date;
    private type: CommentType;

    private title: string;

    public constructor(value: string, type: CommentType, title?: string) {
        this.dateAdded = new Date();
        this.value = value;
        this.type = type;
        this.title = title || '';
    }

    public getValue() {
        return this.value;
    }

    public getType() {
        return this.type;
    }

    public getDateAdded() {
        return this.dateAdded;
    }

    public getTitle() {
        return this.title;
    }
}

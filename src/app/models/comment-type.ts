export enum CommentType {
    'Complain',
    'Appraisal',
    'Suggestion'
}

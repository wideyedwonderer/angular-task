# AngularTask

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.3.

## Additional Features

1. Client-side routing.
2. Reactive forms.
3. Fake route resolver - in order to show the spinner.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

